 function initTabs() {
    $('#content_2').hide();
};

// Выбор вкладки на странице товара
function tabSwitch(nb) {
    $('#tabs a').removeClass('active');
    $('div.tab-content').hide();
    $('#tab_' + nb).addClass('active');
    $('#content_' + nb).show();
};

function clearAll() {
    $('textarea[name="input_text"]').val('');
    $('.text2').empty();
    $('.stats').empty();
    $('.progress-bar').html("0%");
    $('.progress-bar').css("width", "0%")
};
$(function() { initTabs(); });

function pressedButton(nb) {
    $('#button').css('background-color', "#3D9200");
    $('input[value="' + nb + '"]').css( "background-color", "#3D9200" );
};
