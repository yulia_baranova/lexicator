Одни ее едят. Другие брезгуют. Но, тайком все равно едят.
Речь о шаверме.
Шаверма – это международное блюдо.
Гамбургер. Хот-дог. Пицца. Сэндвич. Паста. Шаверма.
Это всемирно известные блюда и шаверма стоит в одном ряду с ними...
Всем привет! Это я, Леонид  leopold1984, начинаю вахту в блоге Олега!
...Эти блюда являются очень популярными в разных заведениях по нескольким причинам:
1. Много калорий (булка, мясо, все дела)
2. Не надо думать о прожарке, гарнире и т.д. Это вещь в себе.
У нас в Питере ее называют – шаверма. В Москве шаурма. За рубежом она известна под разными названиями: Донер или Донер кебаб.
Я обожаю шаверму. Я ее обожаю настолько, что три года назад мы ввели ее в наших ресторанах в меню. Она сразу же вышла на первое место по продажам и удерживает его до сих пор. Сейчас у нас в меню 12 видов шаверм.
Когда-то, когда мы думали и сомневались: вводить/не вводить (все-таки стремное это дело – вводить блюдо с таким непонятным имиджем в меню приличного заведения), мы опросили ряд людей и кое-что выяснили. Оказывается, все люди делятся на две категории: одни обожают шаверму, другие говорят, что это отрава, но, тайком, все равно едят.
У каждого из нас с шавермой связано многое. Особенно из студенчества. У каждого была любимая шаверма у какого-нибудь метро, где почти родной кавказский повар мог специально для тебя сделать «с побольше лука и соуса».
Не знаю как в Москве, но в Питере шаверма есть сейчас в меню каждого пятого заведения. Даже в Гинзовских заведениях в некоторых есть.
А что думаете о шаверме вы?
Расскажите про ваш любимый ларек с шавермой!