#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import codecs
import re
import nltk
import pymorphy2
import os

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
APP_STATIC = os.path.join(APP_ROOT, 'data')

morph = pymorphy2.MorphAnalyzer()
lexicalMinimum = []
upperLexicalMinimum = []
isDictionary = []

#запись лексического минимум указанного уровня из файла в список
def createLexMin(lex_min_list_file_name):
    lexMin = []
    fp = codecs.open(lex_min_list_file_name, 'r', 'utf-8')
    line = fp.readline().encode('utf-8')
    while line:
        word = line.strip()
        lexMin.append(word)
        line = fp.readline().encode('utf-8')
    fp.close()
    return lexMin

#токенизация текста на предложения
def getSentencies(text):
    return nltk.PunktSentenceTokenizer().tokenize(text)

#токенизация предложения на слова
def getWords(sentence):
    sentence = sentence.decode('utf8').replace("«", '"').replace("»", '"') #костыль, в теории нужно вынести в регэксп разбиения по словам, но не работает видимо по причине неправильной кодировки
    words = re.split(r'[\s*\t\n\.\|\:\–\—\/\,\?\!\"()]+', sentence)
    return words[:-1] #удаление точки в конце предложения

def getLemma(word):
    try:
        lemmatizeWord = morph.parse(word.lower())[0]
        return lemmatizeWord.normal_form
    except IndexError:
        return word

def getStatistics(text):
    #список статистических параметров, выводимых в пользовательский интерфейс

    #краткий список
    avWordsInSent = 0
    notInLexMin = 0.0 #проценты
    notInUpperLexMin = 0.0 #проценты
    awWordLenInChars = 0
    awWordLenInSylls = 0

    #расширенный список

    textLenInChars = 0
    textLenInWords = 0
    avSentLenInSylls = 0
    percOfWordsMore3Sylls = 0.0 #проценты
    percOfWordsMore4Sylls = 0.0 #проценты
    percOfWordsMore5Sylls = 0.0 #проценты
    percOfWordsMore6Sylls = 0.0 #проценты
    avSentLenInChars = 0
    percOfWordsMore5Chars = 0.0 #проценты
    percOfWordsMore6Chars = 0.0 #проценты
    percOfWordsMore7Chars = 0.0 #проценты
    percOfWordsMore8Chars = 0.0 #проценты
    percOfWordsMore9Chars = 0.0 #проценты
    percOfWordsMore10Chars = 0.0 #проценты
    percOfWordsMore11Chars = 0.0 #проценты
    percOfWordsMore12Chars = 0.0 #проценты
    percOfWordsMore13Chars = 0.0 #проценты

    sentenciesList = getSentencies(text)
    wordList = []
    listOfVowels = [u'а', u'у', u'о', u'ы', u'и', u'э', u'я', u'ю', u'ё', u'е']
    textLenInChars = 0 #общее количество букв в словах
    syllCount = 0 #общее число слогов

    for sentence in sentenciesList:
        wordList.extend(getWords(sentence)) #присоединение списка слов каждого предложения к списку всех слов

    for word in wordList:
        textLenInChars += len(word)
        wordInSylls = ""
        for symbol in word:
            if symbol in listOfVowels:
                syllCount += 1
                wordInSylls += symbol

        if len(wordInSylls) >= 3:
            percOfWordsMore3Sylls += 1
        if len(wordInSylls) >= 4:
            percOfWordsMore4Sylls += 1
        if len(wordInSylls) >= 5:
            percOfWordsMore5Sylls += 1
        if len(wordInSylls) >= 6:
            percOfWordsMore6Sylls += 1

        if len(word) >= 5:
            percOfWordsMore5Chars += 1
        if len(word) >= 6:
            percOfWordsMore6Chars += 1
        if len(word) >= 7:
            percOfWordsMore7Chars += 1
        if len(word) >= 8:
            percOfWordsMore8Chars += 1
        if len(word) >= 9:
            percOfWordsMore9Chars += 1
        if len(word) >= 10:
            percOfWordsMore10Chars += 1
        if len(word) >= 11:
            percOfWordsMore11Chars += 1
        if len(word) >= 12:
            percOfWordsMore12Chars += 1
        if len(word) >= 13:
            percOfWordsMore13Chars += 1

        if (getLemma(word).encode('utf-8') not in lexicalMinimum) and (getLemma(word).encode('utf-8') not in isDictionary):
            notInLexMin += 1
        #if (lexicalMinimum != upperLexicalMinimum):
        #    if (getLemma(word).encode('utf-8') not in upperLexicalMinimum) and (getLemma(word).encode('utf-8') not in isDictionary):
        #        notInUpperLexMin += 1

    avWordsInSent = (len(wordList) * 1.0)/len(sentenciesList) #домножение для приведения типов
    notInLexMin = ((notInLexMin * 1.0)/len(wordList))*100
    #if (lexicalMinimum != upperLexicalMinimum):
    #    notInUpperLexMin= ((notInUpperLexMin * 1.0)/len(wordList))*100
    awWordLenInChars = (textLenInChars * 1.0)/len(wordList)
    awWordLenInSylls = (syllCount * 1.0)/len(wordList)
    textLenInWords = len(wordList)
    avSentLenInSylls = (syllCount * 1.0)/len(sentenciesList)

    percOfWordsMore3Sylls = ((percOfWordsMore3Sylls * 1.0)/len(wordList))*100
    percOfWordsMore4Sylls = ((percOfWordsMore4Sylls * 1.0)/len(wordList))*100
    percOfWordsMore5Sylls = ((percOfWordsMore5Sylls * 1.0)/len(wordList))*100
    percOfWordsMore6Sylls = ((percOfWordsMore6Sylls * 1.0)/len(wordList))*100

    avSentLenInChars = (textLenInChars * 1.0)/len(sentenciesList)

    percOfWordsMore5Chars = ((percOfWordsMore5Chars * 1.0)/len(wordList))*100
    percOfWordsMore6Chars = ((percOfWordsMore6Chars * 1.0)/len(wordList))*100
    percOfWordsMore7Chars = ((percOfWordsMore7Chars * 1.0)/len(wordList))*100
    percOfWordsMore8Chars = ((percOfWordsMore8Chars * 1.0)/len(wordList))*100
    percOfWordsMore9Chars = ((percOfWordsMore9Chars * 1.0)/len(wordList))*100
    percOfWordsMore10Chars = ((percOfWordsMore10Chars * 1.0)/len(wordList))*100
    percOfWordsMore11Chars = ((percOfWordsMore11Chars * 1.0)/len(wordList))*100
    percOfWordsMore12Chars = ((percOfWordsMore12Chars * 1.0)/len(wordList))*100
    percOfWordsMore13Chars = ((percOfWordsMore13Chars * 1.0)/len(wordList))*100
    #print notInUpperLexMin
    return avWordsInSent, notInLexMin, awWordLenInChars, awWordLenInSylls, textLenInChars, textLenInWords, avSentLenInSylls, percOfWordsMore3Sylls, percOfWordsMore4Sylls, percOfWordsMore5Sylls, percOfWordsMore6Sylls, avSentLenInChars, percOfWordsMore5Chars, percOfWordsMore6Chars, percOfWordsMore7Chars, percOfWordsMore8Chars, percOfWordsMore9Chars, percOfWordsMore10Chars, percOfWordsMore11Chars, percOfWordsMore12Chars, percOfWordsMore13Chars

def getIndices(lexicalLevel, avWordsInSent, notInLexMin, awWordLenInChars, awWordLenInSylls, textLenInChars, textLenInWords, avSentLenInSylls, percOfWordsMore3Sylls, percOfWordsMore4Sylls, percOfWordsMore5Sylls, percOfWordsMore6Sylls, avSentLenInChars, percOfWordsMore5Chars, percOfWordsMore6Chars, percOfWordsMore7Chars, percOfWordsMore8Chars, percOfWordsMore9Chars, percOfWordsMore10Chars, percOfWordsMore11Chars, percOfWordsMore12Chars, percOfWordsMore13Chars):
    pass



def getParameters(text, lexicalLevel):
    global lexicalMinimum
    global upperLexicalMinimum
    pathToLexMin = {'A1':"lexmin0.txt", 'A2':"lexmin1.txt", 'B1':"lexmin2.txt"}.get(lexicalLevel)
    #if (lexicalLevel != "B1"):
    lexicalMinimum = createLexMin(os.path.join(APP_STATIC, pathToLexMin)) #ссылка на файл с лексическим словарем - lexmin0.txt, lexmin1.txt, lexmin2.txt - словари по уровням elementary, basic, first
    #else:
    #    upperLexicalMinimum = createLexMin(os.path.join(APP_STATIC, "lexmin2.txt")) #лексика самого высокого уровня используется при определении количества слов, не входящих в "простые" при расчёте индексов
    #    lexicalMinimum = upperLexicalMinimum
    return getStatistics(text)

def getLexicalMarkup(text, lexicalLevel):
    global lexicalMinimum
    createIsDictionary()
    markupWords = []
    textWithLexicalMarkup = " " + text.decode('utf8')
    pathToLexMin = {'A1':"lexmin0.txt", 'A2':"lexmin1.txt", 'B1':"lexmin2.txt"}.get(lexicalLevel)
    lexicalMinimum = createLexMin(os.path.join(APP_STATIC, pathToLexMin))
    isList = createIsAndModList(text)
    numsAndLatinList=createNumsListAndLatinList(text)
    for sentence in getSentencies(text):
        for word in getWords(sentence):
            lemmWord = getLemma(word).encode('utf-8')
            if (lemmWord not in lexicalMinimum) and (lemmWord not in markupWords) and (lemmWord.decode('utf-8') not in isList) and (lemmWord.decode('utf-8') not in numsAndLatinList):
                markupWords.append(lemmWord)
                pattern = re.compile(r"[\s\t\n\.\|\:\«\»\–\—\/\,\?\!\"()]{1}" + word + r"[\s\t\n\.\|\:\–\—\/\,\?\!\"()]{1}")
                match = pattern.search(textWithLexicalMarkup)
                while match:
                    textWithLexicalMarkup = textWithLexicalMarkup[:match.start()+1] + '<span class="lexmarkup">' + word + '</span>' + textWithLexicalMarkup[match.end()-1:]
                    match = pattern.search(textWithLexicalMarkup)
    return textWithLexicalMarkup

#создание словаря с именами собственными
def createIsDictionary():
    global isDictionary
    fp = codecs.open(os.path.join(APP_STATIC, "slov_is.txt"), 'r', 'utf-8')
    line = fp.readline()
    while line:
        word = line.strip()
        isDictionary.append(word.lower().encode('utf-8'))
        line = fp.readline()
    fp.close()
    return

#запись с именами собственными для ручного модерирования
def writeModDictionary(modDictionary):
    fp = codecs.open(os.path.join(APP_STATIC, "for_mod.txt"), 'r', 'utf-8')
    currentModDict = [] # для проверки на то, присутствует ли в файле аналогичное ИС
    line = fp.readline()
    while line:
        word = line.strip()
        currentModDict.append(word.lower().encode('utf-8'))
        line = fp.readline()
    fp.close()
    fp = codecs.open(os.path.join(APP_STATIC, "for_mod.txt"), 'a', 'utf-8')
    for word in modDictionary:
        if word.encode('utf-8') not in currentModDict:
            fp.write(word + "\n");
    fp.close()
    return

def createIsAndModList(text):
    global isDictionary
    isList = []
    modList = []
    pattern = re.compile(r"[А-ЯЁ]+[а-яё]+")
    match = pattern.findall(text.decode('utf8'))
    for word in match:
        lemmWord = getLemma(word)
        if lemmWord.encode('utf-8') in isDictionary:
            isList.append(lemmWord)
        elif lemmWord.encode('utf-8') not in lexicalMinimum:
            modList.append(lemmWord)
    writeModDictionary(set(modList))
    return set(isList)

def createNumsListAndLatinList(text):
    lt = []
    pattern = re.compile(r'[A-Za-z0-9]+')
    match = pattern.findall(text.decode('utf8').lower())
    lt.extend(match)
    return lt
