#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from flask import Flask
from flask import render_template
from flask import request
import textParameters


app = Flask(__name__)
app.config.from_object('config')

@app.route('/')
@app.route('/index')
def index():
    return render_template("index.html")

@app.route('/', methods = ['POST'])
def signup():
    text = request.form['input_text']
    if (len(text.strip()) > 0 or len(re.split(r'[\s*\t\n\.\|\:\–\/\,\?\!\"()]+', text.strip())>2)):
        lexicalLevel = request.form['button'].encode('utf8')
        textWithLexicalMarkup = textParameters.getLexicalMarkup(text.encode('utf8'), lexicalLevel)
        avWordsInSent, notInLexMin, awWordLenInChars, awWordLenInSylls, textLenInChars, textLenInWords, avSentLenInSylls, percOfWordsMore3Sylls, percOfWordsMore4Sylls, percOfWordsMore5Sylls, percOfWordsMore6Sylls, avSentLenInChars, percOfWordsMore5Chars, percOfWordsMore6Chars, percOfWordsMore7Chars, percOfWordsMore8Chars, percOfWordsMore9Chars, percOfWordsMore10Chars, percOfWordsMore11Chars, percOfWordsMore12Chars, percOfWordsMore13Chars = textParameters.getParameters(text.encode('utf8'), lexicalLevel)
        textWithLexicalMarkup = textParameters.getLexicalMarkup(text.encode('utf8'), lexicalLevel)
        return render_template("index.html", lexText = textWithLexicalMarkup, input = text, avWordsInSent = round(avWordsInSent, 2), notInLexMin = round(notInLexMin, 2), awWordLenInChars = round(awWordLenInChars, 2), awWordLenInSylls = round(awWordLenInSylls, 2), textLenInChars = round(textLenInChars, 2), textLenInWords = round(textLenInWords, 2), avSentLenInSylls = round(avSentLenInSylls, 2), percOfWordsMore3Sylls = round(percOfWordsMore3Sylls, 2), percOfWordsMore4Sylls = round(percOfWordsMore4Sylls, 2), percOfWordsMore5Sylls = round(percOfWordsMore5Sylls, 2), percOfWordsMore6Sylls = round(percOfWordsMore6Sylls, 2), avSentLenInChars = round(avSentLenInChars, 2), percOfWordsMore5Chars = round(percOfWordsMore5Chars, 2), percOfWordsMore6Chars = round(percOfWordsMore6Chars, 2), percOfWordsMore7Chars = round(percOfWordsMore7Chars, 2), percOfWordsMore8Chars = round(percOfWordsMore8Chars, 2), percOfWordsMore9Chars = round(percOfWordsMore9Chars, 2), percOfWordsMore10Chars = round(percOfWordsMore10Chars, 2), percOfWordsMore11Chars = round(percOfWordsMore11Chars, 2), percOfWordsMore12Chars = round(percOfWordsMore12Chars, 2), percOfWordsMore13Chars = round(percOfWordsMore13Chars, 2))
    else:
        return render_template("index.html")

@app.route('/about_project', methods = ['GET'])
def aboutProgect():
    return render_template("about_project.html")

@app.route('/usage', methods = ['GET'])
def contacts():
    return render_template("usage.html")